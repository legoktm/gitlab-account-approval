# Copyright (c) 2023 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of GitLab Account Approval Bot.
#
# GitLab Account Approval Bot is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# GitLab Account Approval Bot is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# GitLab Account Approval Bot.  If not, see <http://www.gnu.org/licenses/>.
import json
import logging

import requests

from . import settings

logger = logging.getLogger(__name__)


class APIError(Exception):
    def __init__(self, message, code, result):
        self.message = message
        self.code = code
        self.result = result

    def __str__(self):
        return f"{self.message} ({self.code})"


class Client:
    """Phabricator client"""

    _default_instance = None

    @classmethod
    def default_client(cls):
        """Get a Phabricator client using the default credentials."""
        if cls._default_instance is None:
            logger.debug("Creating default instance")
            cls._default_instance = cls(
                settings.PHABRICATOR_URL,
                settings.PHABRICATOR_USER,
                settings.PHABRICATOR_TOKEN,
            )
        return cls._default_instance

    def __init__(self, url, username, token):
        self.url = url
        self.username = username
        self.session = {
            "token": token,
        }

    def post(self, path, data):
        data["__conduit__"] = self.session
        r = requests.post(
            f"{self.url}/api/{path}",
            data={
                "params": json.dumps(data),
                "output": "json",
            },
        )
        resp = r.json()
        logger.debug("%s result: %s", path, resp)
        if resp["error_code"] is not None:
            raise APIError(
                resp["error_info"],
                resp["error_code"],
                resp.get("result", None),
            )
        return resp["result"]

    def user_ldapquery(self, names):
        """Lookup Phabricator user data associated with LDAP cn values."""
        names = list(filter(None, names))
        try:
            r = self.post(
                "user.ldapquery",
                {
                    "ldapnames": names,
                    "offset": 0,
                    "limit": len(names),
                },
            )
        except APIError as e:
            if (
                e.code == "ERR-INVALID-PARAMETER"
                and "Unknown or missing ldap names" in e.message
            ):
                logger.debug(e.message)
                if e.result is None:
                    raise KeyError(
                        "Users not found for {}".format(", ".join(names)),
                    )
                else:
                    # Return the partial result
                    r = e.result
            else:
                raise e
        else:
            return r

    def user_mediawikiquery(self, names):
        """Lookup Phabricator user data associated with mediawiki accounts."""
        names = list(filter(None, names))
        try:
            r = self.post(
                "user.mediawikiquery",
                {
                    "names": names,
                    "offset": 0,
                    "limit": len(names),
                },
            )
        except APIError as e:
            if (
                e.code == "ERR-INVALID-PARAMETER"
                and "Unknown or missing mediawiki names" in e.message
            ):
                logger.debug(e.message)
                if e.result is None:
                    raise KeyError(
                        "Users not found for {}".format(", ".join(names)),
                    )
                else:
                    # Return the partial result
                    r = e.result
            else:
                raise e
        else:
            return r

    def user_external_lookup(self, ldap=None, mw=None):
        """Lookup Phabricator user data associated with external accounts."""
        r = []
        if ldap is not None and len(ldap) and ldap[0] is not None:
            try:
                r.extend(self.user_ldapquery(ldap))
            except KeyError as e:
                logger.debug(e)
                pass
        if mw is not None and len(mw) and mw[0] is not None:
            try:
                mw_r = self.user_mediawikiquery(mw)
            except KeyError as e:
                logger.debug(e)
                pass
            else:
                # Deep merge results
                for u in mw_r:
                    logger.debug(u)
                    for u2 in r:
                        if u2["phid"] == u["phid"]:
                            # Decorate existing record with found mw username
                            u2["mediawiki_username"] = u["mediawiki_username"]
                            logger.debug(
                                "Added mediawiki username to %s",
                                u2["phid"],
                            )
                            break
                    else:
                        # An else attached to a for loop is reached if a break
                        # was not issued inside the for loop.
                        logger.debug("Fall through for %s", u["phid"])
                        r.append(u)
        return r

    def project_members(self, name):
        """Lookup a list of PHIDs that are members of a given project."""
        r = self.post(
            "project.search",
            {
                "constraints": {"name": name},
                "attachments": {"members": True},
                "limit": 1,
            },
        )
        members = r["data"][0]["attachments"]["members"]["members"]
        return [m["phid"] for m in members]
