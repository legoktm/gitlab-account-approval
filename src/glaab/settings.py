# Copyright (c) 2023 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of GitLab Account Approval Bot.
#
# GitLab Account Approval Bot is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# GitLab Account Approval Bot is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# GitLab Account Approval Bot.  If not, see <http://www.gnu.org/licenses/>.
import pathlib

import environ

env = environ.Env()
env.smart_cast = False
environ.Env.read_env(env_file=pathlib.Path.cwd() / ".env")

# == GitLab settings ==
GITLAB_URL = env.str("GITLAB_URL", default="https://gitlab.wikimedia.org")
GITLAB_ACCESS_TOKEN = env.str("GITLAB_ACCESS_TOKEN")

# == LDAP settings ==
LDAP_SERVERS = env.list(
    "LDAP_SERVERS",
    default=["ldap-ro.eqiad.wikimedia.org"],
)
LDAP_BASEDN = env.str("LDAP_BASEDN", default="dc=wikimedia,dc=org")
LDAP_TRUSTED_GROUPS = env.list(
    "LDAP_TRUSTED_GROUPS",
    default=[
        "cn=nda,ou=groups,dc=wikimedia,dc=org",
        "cn=ops,ou=groups,dc=wikimedia,dc=org",
        # "cn=project-bastion,ou=groups,dc=wikimedia,dc=org",
        "cn=project-tools,ou=groups,dc=wikimedia,dc=org",
        "cn=wmde,ou=groups,dc=wikimedia,dc=org",
        "cn=wmf,ou=groups,dc=wikimedia,dc=org",
    ],
)

# == Phabricator settings ==
PHABRICATOR_URL = env.str(
    "PHABRICATOR_URL",
    default="https://phabricator.wikimedia.org",
)
PHABRICATOR_USER = env.str("PHABRICATOR_USER", default="glaab")
PHABRICATOR_TOKEN = env.str("PHABRICATOR_TOKEN")
PHABRICATOR_TRUSTED_GROUP = env.str(
    "PHABRICATOR_TRUSTED_GROUP",
    default="Trusted-Contributors",
)

# == MediaWik settings ==
MEDIAWIKI_HOST = env.str("MEDIAWIKI_HOST", default="wikitech.wikimedia.org")
MEDIAWIKI_CONSUMER_TOKEN = env.str("MEDIAWIKI_CONSUMER_TOKEN")
MEDIAWIKI_CONSUMER_SECRET = env.str("MEDIAWIKI_CONSUMER_SECRET")
MEDIAWIKI_ACCESS_TOKEN = env.str("MEDIAWIKI_ACCESS_TOKEN")
MEDIAWIKI_ACCESS_SECRET = env.str("MEDIAWIKI_ACCESS_SECRET")
MEDIAWIKI_LOG_PAGE = env.str(
    "MEDIAWIKI_LOG_PAGE",
    default="Tool:Gitlab-account-approval/Log",
)

# == Gerrit settings ==
GERRIT_URL = env.str("GERRIT_URL", default="https://gerrit.wikimedia.org/r")
GERRIT_USER = env.str("GERRIT_USER", default="gitlabaccountapprovalbot")
GERRIT_PASSWORD = env.str("GERRIT_PASSWORD")
GERRIT_TRUSTED_GROUP = env.str(
    "GERRIT_TRUSTED_GROUP",
    default="Trusted-Contributors",
)
