# Copyright (c) 2023 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of GitLab Account Approval Bot.
#
# GitLab Account Approval Bot is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# GitLab Account Approval Bot is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# GitLab Account Approval Bot.  If not, see <http://www.gnu.org/licenses/>.
import logging

import click
import coloredlogs

from . import gitlab
from . import mediawiki
from . import utils
from .version import __version__

logger = logging.getLogger(__name__)


@click.command()
@click.version_option(version=__version__)
@click.option(
    "-v",
    "--verbose",
    count=True,
    help="Increase debug logging verbosity",
)
@click.option(
    "--dry-run",
    is_flag=True,
    default=False,
    help="Do not actually change anything",
)
def main(verbose, dry_run):
    """Approve pending GitLab accounts for trusted contributors."""

    coloredlogs.install(
        level=max(logging.DEBUG, logging.WARNING - (10 * verbose)),
        fmt="%(asctime)s %(name)s %(levelname)s: %(message)s",
        datefmt="%Y-%m-%dT%H:%M:%SZ",
        level_styles=coloredlogs.DEFAULT_LEVEL_STYLES
        | {
            "debug": {},
            "info": {"color": "green"},
        },
        field_styles=coloredlogs.DEFAULT_FIELD_STYLES
        | {
            "asctime": {"color": "yellow"},
        },
    )
    logging.captureWarnings(True)

    gl = gitlab.Client.default_client()
    mw = mediawiki.Client.default_client()

    click.echo(click.style("Searching for pending accounts...", fg="yellow"))
    for gitlab_user in gl.users_pending_approval():
        username = gitlab_user["username"]
        if utils.is_trusted(gitlab_user):
            if not dry_run:
                gl.approve_user(gitlab_user)
                mw.log_account_approval(gitlab_user)
            else:
                logger.warning(
                    "Cowardly refusing to approve user %s.",
                    username,
                )
            click.echo(click.style(f"{username}: Trusted", fg="green"))
        else:
            click.echo(click.style(f"{username}: Untrusted", fg="red"))


if __name__ == "__main__":  # pragma: nocover
    main()
