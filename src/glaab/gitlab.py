# Copyright (c) 2023 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of GitLab Account Approval Bot.
#
# GitLab Account Approval Bot is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# GitLab Account Approval Bot is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# GitLab Account Approval Bot.  If not, see <http://www.gnu.org/licenses/>.
import json
import logging

import requests

from . import settings

logger = logging.getLogger(__name__)


class APIError(Exception):
    def __init__(self, message, code, result):
        self.message = message
        self.code = code
        self.result = result

    def __str__(self):
        return f"{self.message} ({self.code})"


class Client:
    """GitLab client."""

    _default_instance = None

    @classmethod
    def default_client(cls):
        """Get a GitLab client using the default credentials."""
        if cls._default_instance is None:
            logger.debug("Creating default instance")
            cls._default_instance = cls(
                settings.GITLAB_URL,
                settings.GITLAB_ACCESS_TOKEN,
            )
        return cls._default_instance

    def __init__(self, url, token):
        """Initialize instance."""
        self.url = url
        self.token = token
        self.session = requests.Session()
        self.headers = {
            "PRIVATE-TOKEN": self.token,
            "Content-Type": "application/json",
            "User-Agent": "{name} ({url}) python-requests/{vers}".format(
                name="GitLab Account Approval Bot",
                url="https://wikitech.wikimedia.org/wiki/Tool:Gitlab-account-approval",
                vers=requests.__version__,
            ),
        }

    def http_request(self, verb, path, payload=None, params=None):
        url = f"{self.url}/api/v4/{path}"
        return self.session.request(
            method=verb,
            url=url,
            headers=self.headers,
            params=params,
            json=payload,
        )

    def json_request(self, verb, path, payload=None, params=None):
        r = self.http_request(verb, path, payload, params)
        if 200 <= r.status_code < 300:
            return r.json()

        err_msg = r.content
        try:
            err_json = r.json()
            if "message" in err_json:
                err_msg = err_json["message"]
            if "error" in err_json:
                err_msg = err_json["error"]
        except json.decoder.JSONDecodeError:
            logger.exception(
                "Failed to parse error message from %s/api/v4/%s: %s",
                self.url,
                path,
                err_msg,
            )
        raise APIError(err_msg, r.status_code, r)

    def post(self, path, payload=None):
        resp = self.json_request("POST", path, payload=payload)
        logger.debug("POST %s: %s", path, resp)
        return resp

    def get(self, path, params=None):
        resp = self.json_request("GET", path, params=params)
        logger.debug("GET %s: %s", path, resp)
        return resp

    def _paginated(self, path, params=None, filter=None):
        """Get a generator over items from a paginated endpoint."""
        r = self.http_request("GET", path, params=params)
        while r:
            r.raise_for_status()
            for item in r.json():
                if filter is None or filter(item):
                    yield item
            if not r.links.get("next"):
                break
            r = self.session.request(
                method="GET",
                url=r.links["next"]["url"],
                headers=self.headers,
            )

    def users_pending_approval(self):
        """Get a generator over GitLab accounts pending approval."""
        return self._paginated(
            "users",
            {
                # Cognative danger: x=false parameters are ignored by the
                # service, so setting things like "active=false" only serves
                # to confuse humans.
                "exclude_external": "true",
                "exclude_internal": "true",
                "without_project_bots": "true",
                "per_page": 100,
            },
            # Upstream: <https://gitlab.com/gitlab-org/gitlab/-/issues/276195>
            filter=lambda u: u["state"] == "blocked_pending_approval",
        )

    def approve_user(self, gitlab_user):
        """Mark a user as approved."""
        uid = gitlab_user["id"]
        username = gitlab_user["username"]
        try:
            self.post(f"users/{uid}/approve")
            return True
        except APIError:
            logger.exception(f"Failed to approve user {uid} ({username})")
            return False
