# Copyright (c) 2023 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of GitLab Account Approval Bot.
#
# GitLab Account Approval Bot is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# GitLab Account Approval Bot is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# GitLab Account Approval Bot.  If not, see <http://www.gnu.org/licenses/>.
import logging

import ldap3
import ldap3.utils.log

from . import settings

logger = logging.getLogger(__name__)
ldap3.utils.log.set_library_log_detail_level(ldap3.utils.log.BASIC)
ldap3.utils.log.set_library_log_hide_sensitive_data(True)


class Client:
    """LDAP client."""

    _default_instance = None

    @classmethod
    def default_client(cls):
        """Get an LDAP client using the default credentials."""
        if cls._default_instance is None:
            logger.debug("Creating default instance")
            cls._default_instance = cls(
                settings.LDAP_SERVERS,
                settings.LDAP_BASEDN,
            )
        return cls._default_instance

    def __init__(self, servers, basedn):
        pool = ldap3.ServerPool(
            [ldap3.Server(s, connect_timeout=1) for s in servers],
            ldap3.ROUND_ROBIN,
            active=True,
            exhaust=True,
        )
        self.conn = ldap3.Connection(
            pool,
            auto_bind=ldap3.AUTO_BIND_TLS_BEFORE_BIND,
            lazy=True,
            raise_exceptions=True,
            read_only=True,
            receive_timeout=60,
        )
        self.basedn = basedn

    def developer(self, cn):
        """Lookup a Developer account by `cn`."""
        logger.debug("Searching LDAP for cn=%s", cn)
        return self.conn.extend.standard.paged_search(
            self.basedn,
            f"(&(objectclass=person)(cn={cn}))",
            attributes=["*", "+"],
            paged_size=1,
            time_limit=5,
            generator=False,
        )[0]
