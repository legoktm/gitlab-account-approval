# Copyright (c) 2023 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of GitLab Account Approval Bot.
#
# GitLab Account Approval Bot is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# GitLab Account Approval Bot is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# GitLab Account Approval Bot.  If not, see <http://www.gnu.org/licenses/>.
import json
import logging

import requests

from . import settings

logger = logging.getLogger(__name__)


class APIError(Exception):
    def __init__(self, message, code, result):
        self.message = message
        self.code = code
        self.result = result

    def __str__(self):
        return f"{self.message} ({self.code})"


class RESTClient:
    """Gerrit REST client"""

    _default_instance = None

    @classmethod
    def default_client(cls):
        """Get a Gerrit client using the default credentials."""
        if cls._default_instance is None:
            logger.debug("Creating default instance")
            cls._default_instance = cls(
                settings.GERRIT_URL,
                settings.GERRIT_USER,
                settings.GERRIT_PASSWORD,
            )
        return cls._default_instance

    def __init__(self, url, username, password):
        self.url = url
        self.session = requests.Session()
        self.session.auth = (username, password)
        self.headers = {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Accept-Encoding": "gzip",
            "User-Agent": "{name} ({url}) python-requests/{vers}".format(
                name="GitLab Account Approval Bot",
                url="https://wikitech.wikimedia.org/wiki/Tool:Gitlab-account-approval",
                vers=requests.__version__,
            ),
        }

    def _request(self, verb, path, payload=None, params=None):
        url = f"{self.url}/a/{path}"
        r = self.session.request(
            method=verb,
            url=url,
            headers=self.headers,
            params=params,
            json=payload,
        )
        if r.status_code > 299:
            raise APIError(
                f"Got response {r.status_code} for {url}: {r.content}",
                r.status_code,
                r.content,
            )
        # Remove the weird anti-XSSI prefix string from the response
        body = r.text.lstrip(")]}'")
        logger.debug("%s %s: %s", verb, path, r)
        return json.loads(body)

    def get(self, path, params=None):
        return self._request("GET", path, params=params)

    def post(self, path, payload=None):
        return self._request("POST", path, payload=payload)

    def all_members(self, group):
        """Get a dict(uid:email) of all members of a group."""
        return {
            m["username"]: m["email"]
            for m in self.get(f"groups/{group}/members?recursive")
        }
