GitLab Account Approval Bot
===========================

Do the needful to approve GitLab accounts of folks who are already considered
trusted contributors in other Wikimedia systems.

Deploy on Toolforge
-------------------
```
$ ssh dev.toolforge.org
$ become gitlab-account-approval
$ toolforge envvars create GITLAB_ACCESS_TOKEN '...'
$ toolforge envvars create PHABRICATOR_TOKEN '...'
$ toolforge envvars create MEDIAWIKI_CONSUMER_TOKEN '...'
$ toolforge envvars create MEDIAWIKI_CONSUMER_SECRET '...'
$ toolforge envvars create MEDIAWIKI_ACCESS_TOKEN '...'
$ toolforge envvars create MEDIAWIKI_ACCESS_SECRET '...'
$ toolforge build start \
  https://gitlab.wikimedia.org/toolforge-repos/gitlab-account-approval
$ toolforge jobs run --wait \
  --image tool-gitlab-account-approval/tool-gitlab-account-approval:latest \
  --command 'dry-run' dry-run; \
  toolforge jobs logs dry-run
```

License
-------
[GPL-3.0-or-later](https://www.gnu.org/licenses/gpl-3.0.html)
